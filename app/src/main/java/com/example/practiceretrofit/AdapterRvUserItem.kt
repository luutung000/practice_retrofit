package com.example.practiceretrofit

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.practiceretrofit.databinding.UserItemBinding
import com.example.practiceretrofit.model.DataUser
import com.example.practiceretrofit.model.ListUser

class AdapterRvUserItem: RecyclerView.Adapter<AdapterRvUserItem.ViewHolderRvUserItem>() {
    private var mListUser: List<DataUser> = arrayListOf()
    fun receiverData(dataUser: List<DataUser>){
        mListUser = dataUser
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderRvUserItem {
        return ViewHolderRvUserItem(
            UserItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolderRvUserItem, position: Int) {
        holder.onBind(mListUser[position])
    }

    override fun getItemCount(): Int= mListUser.size

    inner class ViewHolderRvUserItem(private val binding: UserItemBinding): RecyclerView.ViewHolder(binding.root){
        fun onBind(dataUser: DataUser){
            itemView.run {
                binding.tvUserItemId.text = dataUser.id.toString()
                binding.tvUserItemEmail.text = dataUser.email.toString()
                binding.tvUserItemFirstName.text = dataUser.first_name.toString()
                binding.tvUserItemLastName.text = dataUser.last_name
            }
        }
    }

}