package com.example.practiceretrofit.model

data class Support(
    val text: String = "",
    val url: String = ""
)