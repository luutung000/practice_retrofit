package com.example.practiceretrofit.model

data class ListUser(
    val `data`: List<DataUser> = arrayListOf(),
    val page: Int = 0,
    val per_page: Int = 0,
    val support: Support = Support(),
    val total: Int = 0,
    val total_pages: Int = 0
)