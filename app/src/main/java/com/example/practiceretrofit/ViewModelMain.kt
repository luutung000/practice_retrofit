package com.example.practiceretrofit

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.practiceretrofit.model.DataUser
import com.example.practiceretrofit.model.ListUser
import com.example.practiceretrofit.model.Support
import com.example.practiceretrofit.retrofit.RetrofitClient
import com.example.practiceretrofit.retrofit.RetrofitServer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class ViewModelMain: ViewModel() {
    val api = RetrofitClient.createRetrofitClient()
    val dataPost = MutableStateFlow<DataUser>(DataUser())
    val dataPut = MutableStateFlow<DataUser>(DataUser())
    val dataDelete = MutableStateFlow<DataUser>(DataUser())
    val dataPatch = MutableStateFlow<DataUser>(DataUser())
    val dataListUser = MutableStateFlow<ListUser>(ListUser())
    fun postUser(dataUser: DataUser) = viewModelScope.launch(Dispatchers.IO) {
        val responsePostUser = api.postUser(dataUser)
        if (responsePostUser.isSuccessful && responsePostUser.body() != null){
            dataPost.value = responsePostUser.body()!!
        }else{
            val dataUserNull = DataUser( email = "NULL", first_name = "NULL", id = 0, last_name = "NULL")
            dataPost.value = dataUserNull
        }
    }


    fun putUser(id: Int, dataUser: DataUser) = viewModelScope.launch(Dispatchers.IO) {
        val newUser = api.putUser(id,dataUser)
        if (newUser.isSuccessful && newUser.body() != null){
            dataPut.value = newUser.body()!!
        }else{
            val dataUserNull = DataUser( email = "NULL", first_name = "NULL", id = 0, last_name = "NULL")
            dataPut.value = dataUserNull
        }
    }

    fun patchUser(id: Int,dataUser: DataUser) = viewModelScope.launch(Dispatchers.IO) {
        val newUser = api.putUser(id,dataUser)
        if (newUser.isSuccessful && newUser.body() != null){
            dataPatch.value = newUser.body()!!
        }else{
            val dataUserNull = DataUser( email = "NULL", first_name = "NULL", id = 0, last_name = "NULL")
            dataPatch.value = dataUserNull
        }
    }

    fun deleteUser(id: Int) = viewModelScope.launch(Dispatchers.IO) {
        val newUser = api.deleteUser(id)
        if (newUser.isSuccessful && newUser.body() != null){
            dataDelete.value = newUser.body()!!
        }else{
            val dataUserNull = DataUser( email = "NULL", first_name = "NULL", id = 0, last_name = "NULL")
            dataDelete.value = dataUserNull
        }
    }

    fun getListUser() = viewModelScope.launch(Dispatchers.IO) {
        val response = api.getListUser()
        if (response.isSuccessful && response.body() != null){
            dataListUser.value = response.body()!!
        }else{
            val listDataUserDefault = ListUser(data = arrayListOf(),page = 0, per_page = 0, support = Support("NULL","NULL"), total = 0, total_pages = 0)
            dataListUser.value = listDataUserDefault
        }
    }


}