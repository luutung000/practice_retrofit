package com.example.practiceretrofit

class Util {
    companion object{
        const val BASE_URL = "https://reqres.in/"
        const val GET_DATA_POST = 1
        const val GET_DATA_PUT = 2
        const val GET_DATA_PATCH = 3
        const val GET_DATA_DELETE = 4
    }
}