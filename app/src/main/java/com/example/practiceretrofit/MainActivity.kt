package com.example.practiceretrofit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.practiceretrofit.databinding.ActivityMainBinding
import com.example.practiceretrofit.model.DataUser
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val viewModel by lazy {
        ViewModelProvider(this)[ViewModelMain::class.java]
    }
    var checkShowData = 0
    private val listDataUser = ArrayList<DataUser>()
    private lateinit var mAdapterRvUserItem: AdapterRvUserItem
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        mAdapterRvUserItem = AdapterRvUserItem()
        binding.apply {
            rvMain.adapter = mAdapterRvUserItem
            rvMain.layoutManager = LinearLayoutManager(this@MainActivity)

            btnMainGet.setOnClickListener {
                viewModel.apply {
                    when(checkShowData){
                        Util.GET_DATA_POST->{
                            dataPost.onEach {
                                listDataUser.clear()
                                listDataUser.add(it)
                                mAdapterRvUserItem.receiverData(listDataUser)
                                mAdapterRvUserItem.notifyDataSetChanged()
                            }.launchIn(this@MainActivity.lifecycleScope)
                        }
                        Util.GET_DATA_PUT->{
                            dataPut.onEach {
                                listDataUser.clear()
                                listDataUser.add(it)
                                mAdapterRvUserItem.receiverData(listDataUser)
                                mAdapterRvUserItem.notifyDataSetChanged()
                            }.launchIn(this@MainActivity.lifecycleScope)
                        }
                        Util.GET_DATA_PATCH->{
                            dataPatch.onEach {
                                listDataUser.clear()
                                listDataUser.add(it)
                                mAdapterRvUserItem.receiverData(listDataUser)
                                mAdapterRvUserItem.notifyDataSetChanged()
                            }.launchIn(this@MainActivity.lifecycleScope)
                        }
                        Util.GET_DATA_DELETE->{
                            dataDelete.onEach {
                                listDataUser.clear()
                                listDataUser.add(it)
                                mAdapterRvUserItem.receiverData(listDataUser)
                                mAdapterRvUserItem.notifyDataSetChanged()
                            }.launchIn(this@MainActivity.lifecycleScope)
                        }
                    }
                }
            }

            btnMainPost.setOnClickListener {
                checkShowData = Util.GET_DATA_POST
                viewModel.apply {
                    val dataUser = DataUser(
                        id = edMainID.text.toString().trim().toInt(),
                        email = edMainEmail.text.toString().trim(),
                        first_name = edMainFirstName.text.toString().trim(),
                        last_name = edMainLastName.text.toString().trim()
                    )
                    postUser(dataUser)
                }
            }

            btnMainDelete.setOnClickListener {
                checkShowData = Util.GET_DATA_DELETE
                viewModel.apply {
                    deleteUser(2)
                }
            }





        }

    }
}