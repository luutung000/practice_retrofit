package com.example.practiceretrofit.retrofit

import com.example.practiceretrofit.model.DataUser
import com.example.practiceretrofit.model.ListUser
import retrofit2.Response
import retrofit2.http.*

interface RetrofitServer {
    @POST("api/users")
    suspend fun postUser(@Body dataUser: DataUser): Response<DataUser>

    @PUT("api/users/{id}")
    suspend fun putUser(@Path("id") id: Int, @Body dataUser: DataUser): Response<DataUser>

    @PATCH("api/users/{id}")
    suspend fun patchUser(@Path("id") id: Int, @Body dataUser: DataUser): Response<DataUser>

    @DELETE("api/users/{id}")
    suspend fun deleteUser(@Path("id") id: Int): Response<DataUser>

    @GET("api/users")
    suspend fun getListUser(): Response<ListUser>
}